package fr.afpa.service;

import java.util.ArrayList;

import fr.afpa.beans.Connexion;
import fr.afpa.control.GeneralControl;
import fr.afpa.dao.DaoLog;
import fr.afpa.dao.DaoUtilisateur;

public class ServiceLog {
	
	public static boolean login(String login, String mdp) {
		DaoLog dLog = new DaoLog();
		ArrayList<Connexion> listLogs = dLog.recupLogs();

		for (Connexion connexion : listLogs) {
			if (connexion.getLogin().equals(login) && connexion.getMdp().equals(mdp)) {
				GeneralControl.setUtilisateur(DaoUtilisateur.recupUser(connexion));
				return true;
			}
		}
		return false;

	}
	
	public static void allLogin() {
		DaoLog dLog = new DaoLog();
		ArrayList<Connexion> listLogs = dLog.recupLogs();

		for (Connexion connexion : listLogs) {
			System.out.println(connexion);
		}

	}
}
